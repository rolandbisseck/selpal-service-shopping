package za.co.infowarestudios.controller;

/**
 * Created by ROLAND on 1/11/2016.
 */
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.joda.money.Money;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import org.apache.commons.codec.binary.Base64;
import za.co.infowarestudios.RestTemplateProductApplication;
import za.co.infowarestudios.entity.Product;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

public class controlTest
{
    private Logger logger = LoggerFactory.getLogger(RestTemplateProductApplication.class);

    String url = "http://localhost:3000/products";
    List<Product> myProd = new ArrayList<>();

    @Autowired
    Product product;

    @Test
    public void getProducts() {
        try {
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, new HttpEntity<String>(createHeaders("selpal-admin", "selpal-admin-password")), String.class);


            if (HttpStatus.OK == responseEntity.getStatusCode()) {

                logger.info(responseEntity.getBody());

                JsonParser parser = new JsonParser();

                JsonArray jsonArray = (JsonArray) parser.parse(responseEntity.getBody());

                for (JsonElement jsonElement : jsonArray) {
                    JsonObject json = jsonElement.getAsJsonObject();
                    long _id = json.get("id").getAsLong();
                    String title = json.get("name").getAsString();
                    String sku = json.get("sku").getAsString();
                    String price = json.get("price").getAsString();
                    String picture = json.get("image").getAsString();
                    myProd.add(new Product(_id, title, sku, price, picture));

                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    HttpHeaders createHeaders( String username, String password ){
    return new HttpHeaders(){
        {String auth = username + ":" + password;
            byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")) );
            String authHeader = "Basic " + new String( encodedAuth );
            set( "Authorization", authHeader );
            }
        };
    }
}
