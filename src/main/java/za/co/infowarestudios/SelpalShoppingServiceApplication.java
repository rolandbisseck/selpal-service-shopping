package za.co.infowarestudios;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SelpalShoppingServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SelpalShoppingServiceApplication.class, args);
	}
}
