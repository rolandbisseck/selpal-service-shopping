package za.co.infowarestudios.controller;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import za.co.infowarestudios.RestTemplateProductApplication;
import za.co.infowarestudios.entity.Product;
import org.slf4j.Logger;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by ROLAND on 1/7/2016.
 */
@RestController
@CrossOrigin("http://localhost:8100")
@RequestMapping("/products")
public class ControllerProduct {

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<List<Product>> getListProduct()
    {
        List<Product> myProd = new ArrayList<>();

        Logger logger = LoggerFactory.getLogger(RestTemplateProductApplication.class);

        String url = "http://localhost:3000/products";

        try
        {
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<String> responseEntity = restTemplate.exchange(url,HttpMethod.GET,new HttpEntity<String>(createHeaders("selpal-admin","selpal-admin-password")),String.class);


            if(HttpStatus.OK == responseEntity.getStatusCode() ) {
                logger.info("Completed");
                logger.info(responseEntity.getBody());

                JsonParser parser = new JsonParser();

                JsonArray jsonArray = (JsonArray)parser.parse(responseEntity.getBody());

                for (JsonElement jsonElement : jsonArray){
                    JsonObject json = jsonElement.getAsJsonObject();
                    long _id = json.get("id").getAsLong();
                    String title = json.get("name").getAsString();
                    String sku = json.get("sku").getAsString();
                    String price = json.get("price").getAsString();
                    String picture = "";
                    if(!json.get("image").isJsonNull()) {
                        JsonObject imageName = json.get("image").getAsJsonObject();

                        picture = imageName.get("base64").getAsString();
                    }

                    myProd.add(new Product(_id,sku,title,price,picture));
                }

            }
            return new ResponseEntity<List<Product>>(myProd, HttpStatus.OK);

        }
        catch(Exception e)
        {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    HttpHeaders createHeaders( String username, String password ){
        return new HttpHeaders(){
            {String auth = username + ":" + password;
                byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
                String authHeader = "Basic " + new String( encodedAuth );
                set( "Authorization", authHeader );
            }
        };
    }
}
