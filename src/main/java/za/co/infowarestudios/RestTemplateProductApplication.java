package za.co.infowarestudios;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestTemplateProductApplication {

	public static void main(String[] args)
	{
		SpringApplication.run(RestTemplateProductApplication.class, args);
	}
}
