package za.co.infowarestudios.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 * Created by ROLAND on 1/7/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Product
{

    private Long _id;
    private String sku;
    private String title;
    private String price;
    private String picture;

    public Product(String sku, String title, String price, String picture) {
        this.sku = sku;
        this.title = title;
        this.price = price;
        this.picture = picture;
    }

    public Product() {

    }

    public Product(Long _id, String sku, String title, String price, String picture) {
        this._id = _id;
        this.sku = sku;
        this.title = title;
        this.price = price;
        this.picture = picture;
    }

    public Long get_id() {
        return _id;
    }

    public void set_id(Long _id) {
        this._id = _id;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    @Override
    public String toString() {
        return "Value{" +
                "_id=" + _id +
                ", sku='" + sku + '\'' +
                ", title='" + title + '\'' +
                ", price=" + price +
                ", picture='" + picture + '\'' +
                '}';
    }
}
